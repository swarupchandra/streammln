


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class StartForestCover {
	
	public static void main(String[] args) {
		
		Process p = null;
		BufferedReader br = null;
		ProcessHandler ph = null;
		String basepath = "/home/alchemy/";
		int chunkUsed = 0, ensembleCount = 1;
		int maxChunk = 200;
		int totalerror = 0;
		String queryString = "CoverType";
		
		try {
			
			//initial learning
			for(int i=1; i<=ensembleCount; i++) {
				String training = basepath+"stream_mining/training_data/train"+i+".db";
				chunkUsed = i;
				String inputMLN;
				for(int j=1; j<=i; j++) {
					if(j==i) {
						inputMLN = basepath+"stream_mining/stream.mln";
					} else {
						inputMLN = basepath+"stream_mining/ensemble-in/ensemble"+j+".mln";
					}
					String outputMLN = basepath+"stream_mining/ensemble-out/ensemble"+j+".mln";
					
					p = Runtime.getRuntime().exec(new String[]{"bash","-c",basepath + "bin/learnwts "
							+ "-i "+ inputMLN
							+" -o " + outputMLN 
							+" -t " + training
							+" -noAddUnitClauses -dNewton -ne "+queryString});
					System.out.println("Waiting for learnwts  ...");
					ph = new ProcessHandler(p.getInputStream());
					ph.start();
					p.waitFor();
					br = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line;
					while ((line = br.readLine()) != null) {
						System.out.println(line);
					}
				}
				
				//copy output ensemble mln to input ensemble mln
				p = Runtime.getRuntime().exec(new String[]{"bash","-c","rm -f "+basepath+"stream_mining/ensemble-in/*"});
				p.waitFor();
				
				p = Runtime.getRuntime().exec(new String[]{"bash","-c","cp "+basepath+"stream_mining/ensemble-out/* "+basepath+"stream_mining/ensemble-in/"});
				p.waitFor();
				
			}
			
			if(ph != null) {
				ph.done = true;
			}
			
			System.out.println("training done.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(p != null) {
				p.destroy();
			}
			
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		HashMap<String,Double> prevMLN = new HashMap<String, Double>();
		HashMap<String,Integer> decMLN = new HashMap<String,Integer>();
		
		//test on all MLN
		while(chunkUsed < maxChunk) {
			++chunkUsed;
			String evidence = basepath + "stream_mining/testing_data/test"+chunkUsed+".db";
			
			try {
				for(int i=1; i<=ensembleCount; i++) {
					
					String inputMLN = basepath + "stream_mining/ensemble-in/ensemble"+i+".mln";
//					String pruneMLN = basepath + "stream_mining/ensemble-in/ensemble"+i+"A.mln";
//					
//					//prune the input MLN
//					BufferedReader brMLN = null;
//					BufferedWriter bwMLN = null;
//					try {
//						brMLN = new BufferedReader(new FileReader(new File(inputMLN)));
//						String inp = null;
//						int linecount = 0;
//						ArrayList<String> mln = new ArrayList<String>();
//						
//						while((inp = brMLN.readLine()) != null) {
//							++linecount;
//							if(linecount >= 20) {
//								String[] split = inp.split(" ");
//								if(split[0].isEmpty() || split[0].equals("//") ) {
//									mln.add(inp);
//								} else {
//									//don't add if any mln has not changed from the previous.
////									if (Double.parseDouble(split[0]) == 0) {
////										prevMLN.put(inp,"1");
////										mln.add(inp);
////									} else if(prevMLN.get(inp) == null) {
////										prevMLN.put(inp,"1");
////									
//////										if (Double.parseDouble(split[0]) != 0) {
////											mln.add(inp);
//////										}
////									} 
//									
//									//remove whose weight decreases for more than 2 iterations
//									if(Double.parseDouble(split[0]) != 0) {
//										//if not 0, check for value with prevMLN
//										String temp = "";
//										for(int x=1; x<split.length; x++) {
//											temp += split[x];
//										}
//										
//										if(prevMLN.containsKey(temp)) {
//											//prevMLN contains entry, check if current value is less.
//											if(Double.parseDouble(split[0]) < prevMLN.get(temp)) {
//												//if less, check if entry count < 5
//												if(decMLN.get(temp) < 5) {
//													//if entry < 5, add to MLN, else ignore.
//													mln.add(inp);
//												} else {
//													//make the weight to 0
//													String new_tmp = "0\t";
//													for(int x=1; x<split.length; x++) {
//														new_tmp += split[x]+" ";
//													}
//													
//													decMLN.remove(temp);
//													mln.add(new_tmp);
//												}
//											} else {
//												//if value >= current, add to mln
//												mln.add(inp);
//											}
//										} else {
//											//prevMLN does not contain the entry, add to MLN
//											mln.add(inp);
//										}
//									} else {
//										//if 0, then add to MLN
//										mln.add(inp);
//									}
//									
//								}
//							} else {
//								mln.add(inp);
//							}
//						}
//						
//						prevMLN.clear();
//						for(int k=20; k<mln.size(); k++) {
//							String[] split = mln.get(k).split(" ");
//							
//							if(!split[0].isEmpty() && !split[0].startsWith("//") ) {
//								String temp = "";
//								for(int x=1; x<split.length; x++) {
//									temp += split[x];
//								}
//								prevMLN.put(temp,Double.parseDouble(split[0]));
//								if(decMLN.containsKey(temp)) {
//									int tmp = decMLN.get(temp);
//									decMLN.put(temp, ++tmp);
//								} else {
//									decMLN.put(temp, 1);
//								}
//								
//							} 
//							
//							
//						}
//						
//						bwMLN = new BufferedWriter(new FileWriter(new File(pruneMLN)));
//						for(int k=0; k<mln.size(); k++) {
//							bwMLN.write(mln.get(k)+"\n");
//						}
//						
//					} catch (IOException e) {
//						e.printStackTrace();
//						break;
//					} finally {
//						if(brMLN != null) {
//							brMLN.close();
//						}
//						
//						if(bwMLN != null) {
//							bwMLN.close();
//						}
//					}
					
					
					p = Runtime.getRuntime().exec(new String[]{"bash","-c",basepath + "bin/infer -ms"
							+" -i "+ inputMLN
							+" -r " + basepath + "stream_mining/output/out"+i+".result" 
							+" -e " + evidence
							+" -q "+queryString});
					System.out.println("\nInfering class ... MLN "+i);
					ph = new ProcessHandler(p.getInputStream());
					ph.start();
					p.waitFor();
					br = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line;
					while ((line = br.readLine()) != null) {
						System.out.println(line);
					}
				}
				
				if(ph != null) {
					ph.done = true;
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			} finally {
				if(p != null) {
					p.destroy();
				}
				
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			
			//check error
			HashMap<String,String> actualClass = new HashMap<String,String>();
			try {
				br = new BufferedReader(new FileReader(new File(basepath+"stream_mining/class_data/class"+chunkUsed+".db")));
				
				String str = null;
				while((str = br.readLine()) != null) {
					if(str.startsWith(queryString)) {
						String tmp = str.substring(10, str.length()-1);
						String[] split = tmp.split(",");
						actualClass.put(split[0], split[1]);
					} else {
						break;
					}
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			} catch (IOException e) {
				e.printStackTrace();
				break;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			
//			boolean hasError = false;
			for(int i=1; i<=ensembleCount; i++) {
				
				HashMap<String,String> highestClass = new HashMap<String,String>();
				try {
					br = new BufferedReader(new FileReader(new File(basepath+"stream_mining/output/out"+i+".result")));
					
					double outVal = 0;
					String str = null;
					while((str = br.readLine()) != null) {
						if(str.startsWith(queryString)) {
							String[] valSplit = str.split(" ");
							double tmpVal = Double.parseDouble(valSplit[1]);
							String clauseAttr = valSplit[0].substring(10,valSplit[0].length()-1);
							
							String[] split = clauseAttr.split(",");
							
							if(highestClass.get(split[0]) != null) {
								if(tmpVal > outVal) {
									highestClass.put(split[0], split[1]);
									outVal = tmpVal;
								}
							} else {
								highestClass.put(split[0], split[1]);
								outVal = tmpVal;
							}
							
						} else {
							break;
						}
					}
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				} catch (IOException e) {
					e.printStackTrace();
					break;
				} finally {
					if(br != null) {
						try {
							br.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				
				Iterator<String> iter = highestClass.keySet().iterator();
				int error = 0;
				while(iter.hasNext()) {
					String tmp = iter.next();
//					System.out.println(highestClass.get(tmp)+" --- "+actualClass.get(tmp));
					if(!highestClass.get(tmp).equals(actualClass.get(tmp))) {
						++error;
					}
				}
				
//				if(error > 0) {
//					hasError = true;
//				}
				
				System.out.println("\nTotal Error for "+i+" : "+error);
				totalerror += error;
				
				BufferedWriter bw = null;
				try {
					bw = new BufferedWriter(new FileWriter(new File(basepath+"stream_mining/result/error_rate.txt"),true));
					bw.write("Out"+i+": ErrorRate="+((double)error/actualClass.size())+"\n");
				} catch (IOException e) {
					e.printStackTrace();
					break;
				} finally {
					if(bw != null) {
						try {
							bw.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				
				
				
			}
			
			
			
			
			//train MLN and create a new MLN if removed
			if(chunkUsed != maxChunk) {
//				if(hasError) {
					try {
						
						//retraining
						
						String training = basepath+"stream_mining/training_data/train"+chunkUsed+".db";
						for(int j=1; j<=ensembleCount; j++) {
							String inputMLN = basepath+"stream_mining/ensemble-in/ensemble"+j+".mln";	
							String outputMLN = basepath+"stream_mining/ensemble-out/ensemble"+j+".mln";
							
							p = Runtime.getRuntime().exec(new String[]{"bash","-c",basepath + "bin/learnwts "
									+ "-i "+ inputMLN
									+" -o " + outputMLN 
									+" -t " + training
									+" -noAddUnitClauses -dNewton -ne "+queryString});
							System.out.println("Waiting for learnwts  ...");
							ph = new ProcessHandler(p.getInputStream());
							ph.start();
							p.waitFor();
							br = new BufferedReader(new InputStreamReader(p.getInputStream()));
							String line;
							while ((line = br.readLine()) != null) {
								System.out.println(line);
							}
						}
						
						//copy output ensemble mln to input ensemble mln
						p = Runtime.getRuntime().exec(new String[]{"bash","-c","rm -f "+basepath+"stream_mining/ensemble-in/*"});
						p.waitFor();
						
						p = Runtime.getRuntime().exec(new String[]{"bash","-c","cp "+basepath+"stream_mining/ensemble-out/* "+basepath+"stream_mining/ensemble-in/"});
						p.waitFor();
						
						if(ph != null) {
							ph.done = true;
						}
						
//						hasError = false;
						
						System.out.println("training done.");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} finally {
						if(p != null) {
							p.destroy();
						}
						
						if(br != null) {
							try {
								br.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								break;
							}
						}
					}
					
//					break;
//				}
				
			}
			
			
		}
		
		if(ph != null) {
			ph.done = true;
		}
	    
		System.out.println("Total Error Rate = "+((double)totalerror/((double)100000)));
	}
	
}

