import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class MLN {
	public void parse(String mlnfile) {
		
		//for unit testing (comment)
//		String forestcovermln = "/home/alchemy/stream_mining/forestcover/nhybrid_500/ensemble-in/ensemble1.mln";
//		String airlinemln = "/home/alchemy/stream_mining/airline/ensemble-in/ensemble1.mln";
//		String pokermln = "/home/alchemy/stream_mining/poker/ensemble-in/ensemble1.mln";
		
		
		//read file to prune mln before testing on stream
		BufferedReader br = null;
		ArrayList<String> mlnContent = new ArrayList<String>();
		try {
			br = new BufferedReader(new FileReader(new File(mlnfile)));
			String str = null;
//			int linenum = 0;
			while((str = br.readLine()) != null) {
				String[] split = str.split(" ");
				boolean keep = true;
				
				//check for those that start with a number
				if(split.length > 1 && !split[0].startsWith("//") && !split[0].startsWith("!")) {
					
					System.out.println(split[0]+" --- "+split[2]);
					
					//remove those with 0
					if(split[0].equals("0")) {
						keep = false;
					}
				}
				
				if(keep) {
					mlnContent.add(str);
				}
				
				//unit test (comment)
//				++linenum;
//				if(linenum == 30) {
//					break;
//				}
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
					br = null;
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File("ChangedMLN.mln")));
			for(String str : mlnContent) {
				bw.write(str+"\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	//for unit test (comment)
//	public static void main(String[] args) {
//		MLN mln = new MLN();
//		mln.parse();
//	}
}
