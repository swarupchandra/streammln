


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ReadForestCover {
	
	private static final int chunksize = 500;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BufferedReader br = null;
		int initial = 0, numfile = 0;
		int numData = 100000;
		
		try {
			br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/covtype.data")));
			String read = "";
			int count = 0, valID = 0;
			ArrayList<String> largest = new ArrayList<String>(), smallest = new ArrayList<String>();
			
			for(int i=0; i<54; i++) {
				largest.add("-1000.0");
				smallest.add("10000.0");
				
			}
			
			
			ArrayList<String> givenclass = new ArrayList<String>();
			ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
			HashMap<String,Integer> classname = new HashMap<String,Integer>();
			
			
			
			while((read=br.readLine()) != null) {
				
				String[] split = read.split(",");
				
				givenclass.add(split[split.length-1]);
				if(classname.get(split[split.length-1]) == null) {
					classname.put(split[split.length-1], 0);
				} else {
					int i = classname.get(split[split.length-1]);
					classname.put(split[split.length-1], ++i);
				}
				
				ArrayList<String> temp = new ArrayList<String>();
				for(int i=0; i<split.length-1; i++) {
					temp.add(split[i]);
				}
				values.add(temp);
				largest = getLargest(largest, temp);
				smallest = getSmallest(smallest,temp);
				++count;
				
			
				
				if(count != 0 && count%chunksize == 0) {
					
					System.out.println("Count="+count+"; ValueSize="+values.size());
					
					//0=train; 1=test
					++numfile;
//					writefile(0, givenclass, values, initial, numfile);
//					writefile(1, givenclass, values, initial, numfile);
					
					//reset
					givenclass.clear();
//					givenclass = new ArrayList<String>();
					values.clear();
//					values = new HashMap<Integer,ArrayList<String>>();
					valID = 0;
					
					initial += chunksize;
					
					if(initial == numData) {
						Iterator<String> iter = classname.keySet().iterator();
						while(iter.hasNext()) {
							System.out.println("-"+iter.next());
						}
						
						break;
					}
				}
				
				
			}
			
			for(int i=0; i<largest.size(); i++) {
				System.out.println(i+" - "+largest.get(i) + " - " + smallest.get(i) + " - " + (Double.parseDouble(largest.get(i)) + Double.parseDouble(smallest.get(i)))/2);
			}
			
//			for(int i=0; i<largest.size(); i++) {
//				System.out.println("case "+i+": return (int) ((val-("+smallest.get(i)+"))/"+(largest.get(i)-smallest.get(i))+");");
//			}
			
//			for(String temp : classname.keySet()) {
//				System.out.println(temp+" : "+classname.get(temp));
//			}
			
			
			
			
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println("END");
		
		

	}
	
	

//	public static void writefile(int type,ArrayList<String> givenclass, ArrayList<ArrayList<String>> values, int initial, int numfile) {
//		BufferedWriter bw = null;
//		try {
//			
//			HashMap<Integer,Integer> rand = new HashMap<Integer,Integer>();
//			for(int i=0; i<0.4*chunksize; i++) {
//				double rmd = Math.random()*chunksize;
//				rand.put((int) rmd, 1);
//			}
//			
//			if(type == 0) {
//				bw = new BufferedWriter(new FileWriter(new File("training_data/train"+numfile+".db")));
//				
//				
//				for(int i=0; i<givenclass.size(); i++) {
////					
////					if(i>(0.1*chunksize)) {
////						break;
////					}
//					
//					if(rand.containsKey(i)) {
//						String temp = givenclass.get(i);
//						bw.write("CoverType(A"+(i+initial)+","+temp+")\n");
//					}
//					
//					
//				}
//				
//				bw.write("\n");
//				
//			} else {
//				bw = new BufferedWriter(new FileWriter(new File("testing_data/test"+numfile+".db")));
//			}
//			
//			
//			for(int k=0; k<values.get(0).size()-1; k++) {
//				
//				for(int i=0; i<values.size(); i++) {
////					if(type == 0) {
////						if(i>(0.1*chunksize)) {
////							break;
////						}
////					}
//					
//					if(type == 0) {
//						if(!rand.containsKey(i)) {
//							continue;
//						}
//					}
//					
//					switch(k) {
//					case 0: bw.write("Elevation(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 1: bw.write("Aspect(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 2: bw.write("Slope(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 3: bw.write("HorizontalHydrology(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 4: bw.write("VerticalHydrology(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 5: bw.write("HorizontalRoadways(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 6: bw.write("HillshadeNine(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 7: bw.write("HillshadeNoon(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 8: bw.write("HillshadeThree(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 9: bw.write("HorizontalFirePoints(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					default: 
//						if(k >= 10 && k <= 13) {
//							if(values.get(i).get(k).equals("1")) {
//								bw.write("WildernessArea(A"+(i+initial)+","+(k-9)+")\n"); 
//							} else {
//								bw.write("!WildernessArea(A"+(i+initial)+","+(k-9)+")\n"); 
//							}
//						} else if(k >= 14) {
//							if(values.get(i).get(k).equals("1")) {
//								bw.write("SoilType(A"+(i+initial)+","+(k-13)+")\n"); 
//							} else {
//								bw.write("!SoilType(A"+(i+initial)+","+(k-13)+")\n"); 
//							}
//						}
//						break;
//					}
//					
//					
//				}
//				bw.write("\n");
//			}
//			
//			
//			
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			if(bw != null) {
//				try {
//					bw.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		
//		if(type == 0) {
//			
//			try {
//				bw = new BufferedWriter(new FileWriter(new File("class_data/class"+numfile+".db")));
//				
//				for(int i=0; i<givenclass.size(); i++) {
//					
//					String temp = givenclass.get(i);
//					bw.write("CoverType(A"+(i+initial)+","+temp+")\n");
//				}
//				
//				bw.write("\n");
//			} catch (IOException e) {
//				e.printStackTrace();
//			} finally {
//				if(bw != null) {
//					try {
//						bw.close();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			}
//		}
//	}
	
	public static ArrayList<String> getLargest(ArrayList<String> largest, ArrayList<String> value) {
		ArrayList<String> large = new ArrayList<String>();
		for(int i=0; i<value.size(); i++) {
			if(Double.parseDouble(largest.get(i)) < Double.parseDouble(value.get(i))) {
				large.add(value.get(i));
			} else {
				large.add(largest.get(i));
			}
		}
		return large;
	}
	
	public static ArrayList<String> getSmallest(ArrayList<String> smallest, ArrayList<String> value) {
		ArrayList<String> small = new ArrayList<String>();
		for(int i=0; i<value.size(); i++) {
			if(Double.parseDouble(smallest.get(i)) > Double.parseDouble(value.get(i))) {
				small.add(value.get(i));
			} else {
				small.add(smallest.get(i));
			}
		}
		return small;
	}
	
//	public static String getBucket(String val, int pos) {
//		int numbuckets = 100;
//		switch(pos) {
//		case 0: return  Integer.toString((int) ((Double.parseDouble(val)-(1863.0))*numbuckets/1986.0));
//		case 1: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/360.0));
//		case 2: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/61.0));
//		case 3: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/1343.0));
//		case 4: return Integer.toString((int) ((Double.parseDouble(val)-(-146.0))*numbuckets/700.0));
//		case 5: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/7117.0));
//		case 6: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/254.0));
//		case 7: return Integer.toString((int) ((Double.parseDouble(val)-(99.0))*numbuckets/155.0));
//		case 8: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/248.0));
//		case 9: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/7173.0));
//		
//		default: return null;
//		}
//	}
	
}
