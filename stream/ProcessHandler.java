
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ProcessHandler extends Thread {
	InputStream is;
	public boolean done = false;
	
	public ProcessHandler(InputStream is) {
		this.is = is;
	}
	
	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while(!done) {
				if((line = br.readLine()) != null) {
					System.out.println(line);
				}
				
				
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
