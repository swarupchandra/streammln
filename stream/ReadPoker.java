import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class ReadPoker {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BufferedReader br = null;
		int initial = 0, numfile = 0;
		int numData = 100000, chunksize = 500;
		
		try {
			br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/poker.data")));
			String read = "";
			int count = 0, valID = 0;
			ArrayList<String> largest = new ArrayList<String>(), smallest = new ArrayList<String>();
			
			for(int i=0; i<13; i++) {
				largest.add("-10000.0");
				smallest.add("10000.0");
				
			}
			
			
			ArrayList<String> givenclass = new ArrayList<String>();
			ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
			HashMap<String,Integer> classname = new HashMap<String,Integer>();
			
			
			
			while((read=br.readLine()) != null) {
				
				String[] split = read.split(",");
				
				switch(Integer.parseInt(split[split.length-1])) {
					case 0: givenclass.add("Nothing"); break;
					case 1: givenclass.add("OnePair"); break;
					case 2: givenclass.add("TwoPair"); break;
					case 3: givenclass.add("ThreeOfKind"); break;
					case 4: givenclass.add("Straight"); break;
					case 5: givenclass.add("Flush"); break;
					case 6: givenclass.add("FullHouse"); break;
					case 7: givenclass.add("FourOfKind"); break;
					case 8: givenclass.add("StraightFlush"); break;
					case 9: givenclass.add("RoyalFlush"); break;
				}
				
//				if(classname.get(split[split.length-1]) == null) {
//					classname.put(split[split.length-1], 0);
//				} else {
//					int i = classname.get(split[split.length-1]);
//					classname.put(split[split.length-1], ++i);
//				}
				
				ArrayList<String> temp = new ArrayList<String>();
				for(int i=0; i<split.length-1; i++) {
					temp.add(split[i]);
				}
				values.add(temp);
				
//				System.out.println(largest.size()+" ---- " + temp.size());
				
				
				
//				largest = getLargest(largest, temp);
//				smallest = getSmallest(smallest,temp);
				++count;
				
			
				
				if(count != 0 && count%chunksize == 0) {
					
					System.out.println("Count="+count+"; ValueSize="+values.size());
					
					//0=train; 1=test
					++numfile;
					writefile(0, givenclass, values, initial, numfile);
					writefile(1, givenclass, values, initial, numfile);
					
					//reset
					givenclass.clear();
//					givenclass = new ArrayList<String>();
					values.clear();
//					values = new HashMap<Integer,ArrayList<String>>();
					valID = 0;
					
					initial += chunksize;
					
					if(initial == numData) {
						Iterator<String> iter = classname.keySet().iterator();
						while(iter.hasNext()) {
							System.out.println("-"+iter.next());
						}
						
						break;
					}
				}
				
				
			}
//			
//			for(int i=0; i<largest.size(); i++) {
//				System.out.println(i+" - "+largest.get(i) + " - " + smallest.get(i) + " - " + (Double.parseDouble(largest.get(i)) + Double.parseDouble(smallest.get(i)))/2);
//			}
			
//			for(int i=0; i<largest.size(); i++) {
//				System.out.println("case "+i+": return Integer.toString((int) ((Double.parseDouble(val)-"+smallest.get(i)+")*numbuckets/"+(Double.parseDouble(largest.get(i))-Double.parseDouble(smallest.get(i)))+"));");
//			}
			
			for(String temp : classname.keySet()) {
				System.out.println(temp+" : "+classname.get(temp));
			}
			
			
			
			
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println("END");
		
		

	}
	
	

	public static void writefile(int type,ArrayList<String> givenclass, ArrayList<ArrayList<String>> values, int initial, int numfile) {
		BufferedWriter bw = null;
		try {
			
			if(type == 0) {
				bw = new BufferedWriter(new FileWriter(new File("training_data/train"+numfile+".db")));
				
				for(int i=0; i<givenclass.size(); i++) {
					String temp = givenclass.get(i);
					bw.write("PokerHand(A"+(i+initial)+","+temp+")\n");
				}
				
				bw.write("\n");
				
			} else {
				bw = new BufferedWriter(new FileWriter(new File("testing_data/test"+numfile+".db")));
			}
			
			
			for(int k=0; k<values.get(0).size(); k++) {
				for(int i=0; i<values.size(); i++) {
					switch(k) {
//					case 0: bw.write("Year(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 1: bw.write("Aspect(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 2: bw.write("Slope(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 3: bw.write("HorizontalHydrology(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 4: bw.write("VerticalHydrology(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 5: bw.write("HorizontalRoadways(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 6: bw.write("HillshadeNine(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 7: bw.write("HillshadeNoon(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 8: bw.write("HillshadeThree(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
//					case 9: bw.write("HorizontalFirePoints(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n"); break;
					
					case 0: bw.write("S1(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 1: bw.write("C1(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 2: bw.write("S2(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 3: bw.write("C2(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 4: bw.write("S3(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 5: bw.write("C3(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 6: bw.write("S4(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 7: bw.write("C4(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 8: bw.write("S5(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					case 9: bw.write("C5(A"+(i+initial)+", "+values.get(i).get(k)+")\n"); break;
					
					default: 
						
					}
					
					
				}
				bw.write("\n");
			}
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static ArrayList<String> getLargest(ArrayList<String> largest, ArrayList<String> value) {
		ArrayList<String> large = new ArrayList<String>();
		for(int i=0; i<value.size(); i++) {
			if(i==4 || i==5 || i==8 || i==11 || i==12) {
				if(Double.parseDouble(largest.get(i)) < Double.parseDouble(value.get(i))) {
					large.add(value.get(i));
				} else {
					large.add(largest.get(i));
				}
			} else {
				large.add(largest.get(i));
			}
		}
		return large;
	}
	
	public static ArrayList<String> getSmallest(ArrayList<String> smallest, ArrayList<String> value) {
		ArrayList<String> small = new ArrayList<String>();
		for(int i=0; i<value.size(); i++) {
			if(i==4 || i==5 || i==8 || i==11 || i==12) {
				if(Double.parseDouble(smallest.get(i)) > Double.parseDouble(value.get(i))) {
					small.add(value.get(i));
				} else {
					small.add(smallest.get(i));
				}
			} else {
				small.add(smallest.get(i));
			}
		}
		return small;
	}
	
	

}
