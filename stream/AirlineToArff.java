import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;


public class AirlineToArff {
	public static void main(String[] args) {
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/airlines.arff")));
			bw = new BufferedWriter(new FileWriter(new File("/home/swarup/Documents/dataset/airline.data")));
			
//			StringBuffer strbuf = new StringBuffer();
//			strbuf.append("@RELATION AIRLINE\n");
//			strbuf.append("@ATTRIBUTE Week NUMERIC\n");
//			strbuf.append("@ATTRIBUTE DepTime NUMERIC\n");
//			strbuf.append("@ATTRIBUTE ArrTime NUMERIC\n");
//			strbuf.append("@ATTRIBUTE Carrier STRING\n");
//			strbuf.append("@ATTRIBUTE Flight STRING\n");
//			strbuf.append("@ATTRIBUTE ActualArr NUMERIC\n");
//			strbuf.append("@ATTRIBUTE Origin STRING\n");
//			strbuf.append("@ATTRIBUTE Destination STRING\n");
//			strbuf.append("@ATTRIBUTE Distance NUMERIC\n");
//			strbuf.append("@ATTRIBUTE Diverted NUMERIC\n");
//			strbuf.append("@ATTRIBUTE Delayed {Delay,NoDelay}\n\n");
//			strbuf.append("@DATA\n");
//			
//			bw.write(strbuf.toString());
//			
//			String read = "";
//			int count = 0;
//			
//			while((read=br.readLine()) != null) {
//				String[] split = read.split(",");
//				for(int i=3; i<split.length-1; i++) {
//					bw.write(split[i]+",");
//				}
//				if(Double.parseDouble(split[split.length-1]) > 0) {
//					bw.write("Delay");
//				} else {
//					bw.write("NoDelay");
//				}
//				bw.write("\n");
//				++count;
//				if(count == 100000) {
//					break;
//				}
//				
//			}
			
			
			//arff to data
			HashMap<String,Integer> airline = new HashMap<String, Integer>();
			HashMap<String,Integer> airport = new HashMap<String,Integer>();
			String read = "";
			int count = 0, datacount = 0;
			
			while((read=br.readLine()) != null) {
				if(count == 2) {
					String[] split = read.split(" ");
					String data = split[2].substring(1,split[2].length()-1);
					String[] list = data.split(",");
					for(int i=0; i<list.length; i++) {
						airline.put(list[i], i);
					}
				} else if(count == 4) {
					String[] split = read.split(" ");
					String data = split[2].substring(1,split[2].length()-1);
					String[] list = data.split(",");
					for(int i=0; i<list.length; i++) {
						airport.put(list[i].substring(1,list[i].length()-1), i);
					}
				} else if(count > 11) {
					String[] data = read.split(",");
					for(int i=0; i<data.length; i++) {
						if(i==0) {
							bw.write(airline.get(data[i].trim())+",");
						} else if(i==2 || i==3) {
							bw.write(airport.get(data[i].trim())+",");
						} else {
							bw.write(data[i].trim());
							if((i+1) < data.length)
								bw.write(",");
						}
					}
					bw.write("\n");
					++datacount;
				}
				
				++count;
				if(datacount == 100000) {
					break;
				}
			}
			
			
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
