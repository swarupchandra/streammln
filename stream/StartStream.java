

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class StartStream {
	
	private final static String basepath = "/home/alchemy/";
	
	private static String classname = "CoverType";
//	private final static String classname = "Delayed";
//	private static String classname = "PokerHand";
//	private static String classname = "LEDClass";
//	private static String classname = "Class";
	
	private final static int ensembleCount = 1;
	
	private static boolean isMCSAT = false;
	private static int chunksize = 500;
	private static int binSize = 100;
	private static double kldivthreshold = 0.0;
	
	private static boolean isadptbinning = false;
	
	private static boolean issemisupervised = false;
	private static double semipercentlabel = 0.5;
	
	private static final int maxData = 100000;
	private static final int initialTrain = 1;
	
	public static void main(String[] args) {
		isMCSAT = false;
		isadptbinning = true;
		String[] name = {"Delayed"};
		int[] datachunk = {500};
		boolean[] adpt = {true,false};
		
		int c = 0;
		issemisupervised = false;
		semipercentlabel = 0.0;
		kldivthreshold = 0.0;
		
		for(String runclass : name) {
			classname = runclass;
			chunksize = datachunk[c];
			isadptbinning = adpt[c];
			
//			for(int i=1; i<3; i++) {
//				kldivthreshold = i*0.05;
//				startRelationalLearn();
//			}
		
			startRelationalLearn();
			
//			++c;
		}
	}
	
	public static void startRelationalLearn() {
		
//		chunksize = Integer.parseInt(args[1]);
//		binSize = Integer.parseInt(args[2]);
		
		//read input file
		BufferedReader br = null;
		int numfile = 1;
		int numData = maxData;
		StartStream mln = new StartStream();
		
		//split inputfile into smaller chunks
		try {
			ArrayList<String> chunklist = new ArrayList<String>();
//			String inputfile = args[0];
			
			if(classname.equals("CoverType")) {
				br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/covtype.data")));
			} else if(classname.equals("Delayed")) {
				br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/airline.data")));
			} else if(classname.equals("PokerHand")) {
				br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/poker.data")));
			} else if(classname.equals("LEDClass")) {
				br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/syntheticLED.data")));
			} else if(classname.equals("Class")) {
//				br = new BufferedReader(new FileReader(new File("/home/swarup/Desktop/ahsan_dataset/pamap/normalized_ordered_PAMAP2_short_ensemble_testing_set.arff")));
				br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/swarup_please_test_test_ahsan/Syndata_c7_a50_n21_s0_0008_k15_m15k.data")));
			} else {
//				br = new BufferedReader(new FileReader(new File(inputfile)));
			}
			
			String read = "";
			while((read=br.readLine()) != null) {
				//divide file into multiple chunks
				chunklist.add(read);
				--numData;
				if(chunklist.size() == chunksize) {
					mln.writeChunkInput(chunklist,basepath + "stream_mining/inputstream/stream"+numfile+".input");
					++numfile;
					chunklist.clear();
				}
				
				if(numData == 0) {
					if(chunklist.size() > 0) {
						mln.writeChunkInput(chunklist,basepath +"stream_mining/inputstream/stream"+numfile+".input");
					}
					break;
				}
			}
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//structure containing bin size for numeric attributes (largest and smallest per attribute)
		HashMap<Integer,double[]> globalBin = new HashMap<Integer,double[]>();
		
		//Global Distribution structure
		HashMap<Integer,double[]> globalDistribution = null;
		
		int totalerror = 0;
		long starttime = System.currentTimeMillis();
		String errorFile = basepath+"stream_mining/result/error_"+isMCSAT+"_"+classname+"_"+chunksize+"_"+binSize+"_"+kldivthreshold+"_"+isadptbinning+".txt";
		
		double[] prevklscore = null;
		
		//create logic statements
		for(int i=1; i<numfile; i++) {
			String inputfilename = basepath +"stream_mining/inputstream/stream"+i+".input";
			String trainfilename = basepath +"stream_mining/training_data/train"+i+".db";
			String truetrainfilename = basepath +"stream_mining/class_data/train"+i+".db";
			String testfilename = basepath +"stream_mining/testing_data/test"+i+".db";
			
			//read input file
			HashMap<String,ArrayList<String>> chunk = mln.readChunk(inputfilename, (i-1)*chunksize);
			
			//Adaptive Binning = start binning
			HashMap<Integer,double[]> bin = mln.getBin(chunk);
			//check if global bin need change
			globalBin = mln.checkBin(bin,globalBin);
			
			//bin data
			//Adaptive Binning = false
			if(isadptbinning) {
				if(classname.equals("CoverType")) {
					chunk = mln.dataBinForestCover(chunk, globalBin, false);
				} else if(classname.equals("Delayed")) {
					chunk = mln.dataBinAirline(chunk, globalBin, false);
				} else if(classname.equals("PokerHand")) {
					chunk = mln.dataBinPoker(chunk, globalBin, false);
				} else if(classname.equals("LEDClass")) {
					chunk = mln.dataBinLED(chunk, globalBin, false);
				} else {
					chunk = mln.dataBinPAMAP(chunk, globalBin, false);
				}
			} else {
				if(classname.equals("CoverType")) {
					chunk = mln.dataBinForestCover(chunk, globalBin, true);
				} else if(classname.equals("Delayed")) {
					chunk = mln.dataBinAirline(chunk, globalBin, true);
				} else {
					System.out.println("Standard binning not available");
				}
			}
			
			//check if new distribution has changed from the global distribution
			boolean trainRequired = false;
			
			//hybrid
//			boolean trainRequired = true;
			
			HashMap<Integer,double[]> currentDistribution = null;
			if(classname.equals("CoverType")) {
				currentDistribution = mln.getForestCoverDistribution(chunk, globalDistribution);
			} else if(classname.equals("Delayed")) {
				currentDistribution = mln.getAirlineDistribution(chunk, globalDistribution);
			} else if(classname.equals("PokerHand")) {
				currentDistribution = mln.getPokerDistribution(chunk, globalDistribution);
			} else if(classname.equals("LEDClass")) {
				currentDistribution = mln.getLEDDistribution(chunk, globalDistribution);
			} else {
				currentDistribution = mln.getPAMAPDistribution(chunk, globalDistribution);
			}
			
//			HashMap<Integer,double[]> currentDistribution = mln.getForestCoverDistribution(chunk, globalDistribution);
//			HashMap<Integer,double[]> currentDistribution = mln.getAirlineDistribution(chunk, globalDistribution);
//			HashMap<Integer,double[]> currentDistribution = mln.getPokerDistribution(chunk, globalDistribution);
//			HashMap<Integer,double[]> currentDistribution = mln.getPAMAPDistribution(chunk, globalDistribution);
//			HashMap<Integer,double[]> currentDistribution = mln.getLEDDistribution(chunk, globalDistribution);
			
			if(globalDistribution != null) {
				double[] newklscore = new double[currentDistribution.size()];
				
				//check KL Divergence Score for each attribute
				for(Integer attr : currentDistribution.keySet()) {
					newklscore[attr] = mln.klDivergenceScore(globalDistribution.get(attr), currentDistribution.get(attr), i, chunk.size());
				}
				
				if(prevklscore == null) {
					prevklscore = new double[newklscore.length];
				} else {
					for(int kl=0; kl<newklscore.length; kl++) {
						double percentchange = Math.abs((prevklscore[kl] - newklscore[kl])/prevklscore[kl]);
						if(percentchange > kldivthreshold) {
							trainRequired = true;
							break;
						}
					}
				}
				
				for(int kl=0; kl<newklscore.length; kl++) {
					prevklscore[kl] = newklscore[kl];
				}
				
				//if score > threshold, then train
//				if(klScore > kldivthreshold) {
//					trainRequired = true;
//					mln.storeKlScore(-5);
//				}
				
//				mln.storeKlScore(-1);
			}
			
			//set current distribution to reflect global distribution
			if(i <= initialTrain || trainRequired) {
				globalDistribution = currentDistribution;
			}
			
			
			
			//convert to logic form
			if(classname.equals("CoverType")) {
				mln.getForestCoverPredicates(chunk, trainfilename, truetrainfilename, true, issemisupervised);
				mln.getForestCoverPredicates(chunk, testfilename, truetrainfilename, false, issemisupervised);
			} else if(classname.equals("Delayed")) {
				mln.getAirlinePredicates(chunk, trainfilename, truetrainfilename, true, issemisupervised);
				mln.getAirlinePredicates(chunk, testfilename, truetrainfilename, false, issemisupervised);
			} else if(classname.equals("PokerHand")) {
				mln.getPokerPredicates(chunk, trainfilename, truetrainfilename, true, issemisupervised);
				mln.getPokerPredicates(chunk, testfilename, truetrainfilename, false, issemisupervised);
			} else if(classname.equals("LEDClass")) {
				mln.getLEDPredicates(chunk, trainfilename, truetrainfilename, true, issemisupervised);
				mln.getLEDPredicates(chunk, testfilename, truetrainfilename, false, issemisupervised);
			} else {
				if(!issemisupervised) {
					mln.getPAMAPPredicates(chunk, trainfilename, true, issemisupervised);
					mln.getPAMAPPredicates(chunk, testfilename, false, issemisupervised);
				} else {
					System.out.println("Semi-Supervised not available");
					System.exit(-1);
				}
			}
			
			
//			mln.getForestCoverPredicates(chunk, trainfilename, true, false);
//			mln.getForestCoverPredicates(chunk, testfilename, false, false);
//			mln.getAirlinePredicates(chunk, trainfilename, true, false);
//			mln.getAirlinePredicates(chunk, testfilename, false, false);
//			mln.getPokerPredicates(chunk, trainfilename, true, false);
//			mln.getPokerPredicates(chunk, testfilename, false, false);
//			mln.getPAMAPPredicates(chunk, trainfilename, true, false);
//			mln.getPAMAPPredicates(chunk, testfilename, false, false);
//			mln.getLEDPredicates(chunk, trainfilename, true, false);
//			mln.getLEDPredicates(chunk, testfilename, false, false);
			
			//For SemiSupervised
//			mln.getForestCoverPredicates(chunk, trainfilename, true, true);
//			mln.getForestCoverPredicates(chunk, testfilename, false, true);
//			mln.getAirlinePredicates(chunk, trainfilename, true, true);
//			mln.getAirlinePredicates(chunk, testfilename, false, true);
//			mln.getPokerPredicates(chunk, trainfilename, true, true);
//			mln.getPokerPredicates(chunk, testfilename, false, true);
//			mln.getLEDPredicates(chunk, trainfilename, true, true);
//			mln.getLEDPredicates(chunk, testfilename, false, true);
			
			//perform mln mining
//			String trainFile = basepath+"stream_mining/training_data/train"+i+".db";
			ArrayList<String> correct = new ArrayList<String>();
			
			//test
			if(i > initialTrain) {
				String evidence = basepath + "stream_mining/testing_data/test"+i+".db";
				for(int j=1; j<=ensembleCount; j++) {
					String inputMLN = basepath + "stream_mining/ensemble-in/ensemble"+j+".mln";
					String outputResult = basepath + "stream_mining/output/out"+j+".result";
					
					//prune inputMLN for testing
//					String prunedMLN = mln.parse(inputMLN);
					
					mln.test(evidence, inputMLN, outputResult);
//					mln.test(evidence, prunedMLN, outputResult);
					
					//check error
//					String trainFile = basepath+"stream_mining/training_data/train"+i+".db";
					correct = mln.getError(truetrainfilename, outputResult, errorFile);
					int lastindex = correct.size()-1;
					totalerror += Integer.parseInt(correct.get(lastindex));
					correct.remove(lastindex);
				}
			}
			
			
			//train
			if(trainRequired) {
				mln.storePrint(i+": Training Performed");
			} else {
				mln.storePrint(i+": No Training");
			}
			
			
			if(i <= initialTrain || trainRequired) {
				for(int j=1; j<=ensembleCount; j++) {
					String inputMLN = basepath+"stream_mining/stream_"+classname+".mln";
					if(i > 1) {
						inputMLN = basepath+"stream_mining/ensemble-in/ensemble"+j+".mln";
					}
					String outputMLN = basepath+"stream_mining/ensemble-out/ensemble"+j+".mln";
					
//					if(correct.size() > 0) {
//						String newTrainFile = basepath+"stream_mining/training_data/newtrain"+i+".db";
//						mln.getNewTrainFile(correct,trainFile,newTrainFile);
//						mln.train(i, inputMLN, outputMLN, newTrainFile);
//					} else {
						
//						String prunedMLN = mln.parse(inputMLN);
						mln.train(i, inputMLN, outputMLN, trainfilename);
//						mln.train(i, prunedMLN, outputMLN, trainFile);
//					}
					
				}
				
				//test next chunk
				try {
					mln.getNextMLN();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		System.out.println("Total Error Rate = "+((double)totalerror/(double)100000));
		System.out.println("Total time taken = "+(System.currentTimeMillis()-starttime));
		
		mln.finalresult(((double)totalerror/(double)100000), (System.currentTimeMillis()-starttime), errorFile);
		
		
	}
	
	void getNewTrainFile(ArrayList<String> correct, String TrainFile, String newTrainFile) {
		BufferedReader br = null;
		HashMap<String,String> correctMap = new HashMap<String,String>();
		for(int i=0; i<correct.size(); i++) {
			correctMap.put(correct.get(i), "1");
		}
		
		ArrayList<String> newTrain = new ArrayList<String>();
		
		try {
			br = new BufferedReader(new FileReader(new File(TrainFile)));
			
			String str = null;
			while((str = br.readLine()) != null) {
				String[] valSplit = str.split("\\(");
				String[] split = valSplit[1].split(",");
				
				if(correctMap.get(split[0]) != null) {
					newTrain.add(str);
				}
					
				
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
		
		//write new training datapoints to file		
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File(newTrainFile)));
			for(int i=0; i<newTrain.size(); i++) {
				bw.write(newTrain.get(i)+"\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
	}
	
	void finalresult(double totalerror, double timetaken, String errorfile) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File(errorfile),true));
			bw.write("\n\nTotal Error = "+totalerror);
			bw.write("\nTime Taken = "+timetaken);
			bw.write("\n");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	void train(int chunkUsed, String inputMLN, String outputMLN, String training) {
		
		Process p = null;
		BufferedReader br = null;
		ProcessHandler ph = null;
		
		try {		
			//-m = MAP inference and return only positive query atoms
			if(isMCSAT) {
				p = Runtime.getRuntime().exec(new String[]{"bash","-c",basepath + "bin/learnwts"
						+" -i "+ inputMLN
						+" -o " + outputMLN 
						+" -t " + training
						+" -noAddUnitClauses -ne "+classname});
			} else {
				p = Runtime.getRuntime().exec(new String[]{"bash","-c",basepath + "bin/learnwts -m"
						+" -i "+ inputMLN
						+" -o " + outputMLN 
						+" -t " + training
						+" -noAddUnitClauses -ne "+classname});
			}
			
			System.out.println("Waiting for learnwts  ...");
			ph = new ProcessHandler(p.getInputStream());
			ph.start();
			p.waitFor();
			br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			
			
			if(ph != null) {
				ph.done = true;
			}
			
			System.out.println("training done.");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			if(p != null) {
				p.destroy();
			}
			
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
	}
	
	void getNextMLN() throws IOException, InterruptedException {
		
		Process p = null;
		
		//copy output ensemble mln to input ensemble mln
		p = Runtime.getRuntime().exec(new String[]{"bash","-c","rm -f "+basepath+"stream_mining/ensemble-in/*"});
		p.waitFor();
		
		p = Runtime.getRuntime().exec(new String[]{"bash","-c","cp "+basepath+"stream_mining/ensemble-out/* "+basepath+"stream_mining/ensemble-in/"});
		p.waitFor();
	}
	
	
	public String parse(String mlnfile) {
		
		//read file to prune mln before testing on stream
		BufferedReader br = null;
		ArrayList<String> mlnContent = new ArrayList<String>();
		try {
			br = new BufferedReader(new FileReader(new File(mlnfile)));
			String str = null;
			while((str = br.readLine()) != null) {
				String[] split = str.split(" ");
				boolean keep = true;
				
				//check for those that start with a number
				if(split.length > 1 && !split[0].startsWith("//") && !split[0].startsWith("!")) {
										
					//remove those with 0
					if(split[0].equals("0")) {
						keep = false;
					}
				}
				
				if(keep) {
					mlnContent.add(str);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
					br = null;
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		BufferedWriter bw = null;
		String prunedMLN = "prunedTestMLN.mln";
		try {
			bw = new BufferedWriter(new FileWriter(new File(prunedMLN)));
			for(String str : mlnContent) {
				bw.write(str+"\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return prunedMLN;
	}
	
	
	void test(String evidence, String inputMLN, String outputResult) {		
		
		Process p = null;
		BufferedReader br = null;
		ProcessHandler ph = null;
		
		try {
			// -ms = MC-SAT; -m = MAP
			if(isMCSAT) {
				p = Runtime.getRuntime().exec(new String[]{"bash","-c",basepath + "bin/infer -ms"
						+" -i "+ inputMLN
						+" -r " + outputResult
						+" -e " + evidence
						+" -q "+classname});
			} else {
				p = Runtime.getRuntime().exec(new String[]{"bash","-c",basepath + "bin/infer -m"
						+" -i "+ inputMLN
						+" -r " + outputResult
						+" -e " + evidence
						+" -q "+classname});
			}
			
			System.out.println("\nInfering class ... MLN ");
			ph = new ProcessHandler(p.getInputStream());
			ph.start();
			p.waitFor();
			br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			
			
			if(ph != null) {
				ph.done = true;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			if(p != null) {
				p.destroy();
			}
			
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
	}
	
	
	
	ArrayList<String> getError(String trueClassFile, String outputResult, String errorFile) {
		//get actual class labels
		
		HashMap<String,String> actualClass = getActualClassLabel(trueClassFile);
		BufferedReader br = null;
		
			
		HashMap<String,String> highestClass = new HashMap<String,String>();
		try {
			br = new BufferedReader(new FileReader(new File(outputResult)));
			
			int count = 0;
			double outVal = 0;
			String str = null;
			while((str = br.readLine()) != null) {
				++count;
				if(isMCSAT) {
					if(str.startsWith(classname)) {
						String[] valSplit = str.split(" ");
						double tmpVal = Double.parseDouble(valSplit[1]);
						String tmp = valSplit[0].substring(classname.length()+1, valSplit[0].length()-1);
						String[] split = tmp.split(",");
						
						if(highestClass.get(split[0]) != null) {
							if(tmpVal > outVal) {
								highestClass.put(split[0], split[1]);
								outVal = tmpVal;
							}
						} else {
							highestClass.put(split[0], split[1]);
							outVal = tmpVal;
						}
						
					}
				} else {
					if(str.startsWith(classname)) {
						String tmp = str.substring(classname.length()+1, str.length()-1);
						String[] split = tmp.split(",");
						highestClass.put(split[0], split[1]);
					}
				}
			}
			
			if(count == 0) {
				System.out.println("No output result");
				System.exit(-1);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
		
		//get all correctly classified data points
		ArrayList<String> correct = new ArrayList<String>();
		HashMap<String,String> errorDist = new HashMap<String,String>();
		
		Iterator<String> iter = highestClass.keySet().iterator();
		int error = 0;
		while(iter.hasNext()) {
			String tmp = iter.next();
			if(!highestClass.get(tmp).equals(actualClass.get(tmp))) {
				++error;
				errorDist.put(tmp, "-1");
			} else {
				correct.add(tmp);
				errorDist.put(tmp, "1");
			}
		}
		correct.add(error+"");
		
		System.out.println("\nCurrent Error : "+error);
		
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File(errorFile),true));
			bw.write("Out: ErrorRate="+((double)error/actualClass.size())+"\n");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
		
//		try {
//			bw = new BufferedWriter(new FileWriter(new File(errorFile+"dist"),true));
//			for(String str : errorDist.keySet()) {
//				bw.write(str+" "+errorDist.get(str)+"\n");
//			}
//			bw.write("-------------------------------\n\n");
//		} catch (IOException e) {
//			e.printStackTrace();
//			System.exit(0);
//		} finally {
//			if(bw != null) {
//				try {
//					bw.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//					System.exit(0);
//				}
//			}
//		}
		
		return correct;
		
	}
	
	private HashMap<String,String> getActualClassLabel(String chunkfile) {
		HashMap<String,String> actualClass = new HashMap<String,String>();
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new FileReader(new File(chunkfile)));
			
			String str = null;
			while((str = br.readLine()) != null) {
				if(str.startsWith(classname)) {
					String tmp = str.substring(classname.length()+1, str.length()-1);
					String[] split = tmp.split(",");
					actualClass.put(split[0], split[1]);
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
		}
		
		return actualClass;
	}
	
	
	void writeChunkInput(ArrayList<String> chunklist, String inputfile) {
		if(chunklist.size() > 0) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(new File(inputfile)));
				for(int i=0; i<chunklist.size(); i++) {
					bw.write(chunklist.get(i)+"\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(bw != null) {
					try {
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
	}
	
	HashMap<String,ArrayList<String>> readChunk(String inputfilename, int startnum) {
		BufferedReader br = null;
		
		//identifier -> datapoint
		HashMap<String, ArrayList<String>> inputList = new HashMap<String,ArrayList<String>>();
		
		try {
			br = new BufferedReader(new FileReader(new File(inputfilename)));
			String str = "";
			int count = 1;
			while((str=br.readLine()) != null) {
				String[] split = str.split(",");
				ArrayList<String> datapoint = new ArrayList<String>();
				for(int i=0; i<split.length; i++) {
					datapoint.add(split[i].trim());
				}
				inputList.put("A"+startnum+count, datapoint);
				++count;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return inputList;
	}
	
	
	HashMap<Integer,Double> calculateStat(HashMap<String,ArrayList<String>> inputList) {
		//get sum of each attribute to calculate average
		HashMap<Integer,Double> avgVal = new HashMap<Integer,Double>();
		Iterator<String> iter = inputList.keySet().iterator();
		while(iter.hasNext()) {
			ArrayList<String> dataVal = inputList.get(iter.next());
			for(int i=0; i<dataVal.size(); i++) {
				if(avgVal.containsKey(i)) {
					double val = avgVal.get(i);
					avgVal.put(i, val+Double.parseDouble(dataVal.get(i)));
				}
			}
		}
		
		for(int i=0; i<avgVal.size(); i++) {
			double val = avgVal.get(i);
			avgVal.put(i, val/inputList.size());
		}
		
		return avgVal;
	}
	
	
	HashMap<Integer,double[]> getBin(HashMap<String,ArrayList<String>> data) {
		HashMap<Integer,double[]> bin = new HashMap<Integer,double[]>();
		Iterator<String> iter = data.keySet().iterator();
		while(iter.hasNext()) {
			String point = iter.next();
			for(int i=0; i<data.get(point).size()-1; i++) {
				double val = Double.parseDouble(data.get(point).get(i));
				if(bin.get(i) != null) {
					double[] maxmin = bin.get(i);
					if(maxmin[1] > val) {
						maxmin[1] = val;
					} else if(maxmin[0] < val) {
						maxmin[0] = val;
					}
					
				} else {
					double[] maxmin = new double[2];
					maxmin[0] = val;
					maxmin[1] = maxmin[0];
					bin.put(i, maxmin);
				}
			}
		}
		return bin;
	}
	
	HashMap<Integer,double[]> checkBin(HashMap<Integer,double[]> bin, HashMap<Integer,double[]> globalBin) {
		
		if(globalBin.size() == 0) {
			return bin;
		}
		
		HashMap<Integer,double[]> newGlobalBin = new HashMap<Integer,double[]>();
		Iterator<Integer> iter = bin.keySet().iterator();
		while(iter.hasNext()) {
			int index = iter.next();
			double[] newBin = new double[2];
			//largest
			if(bin.get(index)[0] > globalBin.get(index)[0]) {
				newBin[0] = bin.get(index)[0];
			} else {
				newBin[0] = globalBin.get(index)[0];
			}
			
			//smallest
			if(bin.get(index)[1] < globalBin.get(index)[1]) {
				newBin[1] = bin.get(index)[1];
			} else {
				newBin[1] = globalBin.get(index)[1];
			}
			newGlobalBin.put(index, newBin);
		}
		return newGlobalBin;
	}
		
	
	double klDivergenceScore(double[] d1, double[] d2, int chunkNum, int chunkSize) {
		
		double klDiv = 0;
		for(int i=0; i<d1.length; i++) {
			if(d1[i] == 0) { continue; }
			if(d2[i] == 0) { continue; }
			
			d1[i] /= ((chunkNum-1)*chunkSize);
//			d2[i] /= (chunkNum*chunkSize);
			d2[i] /= (chunkSize);
			
			klDiv += (d1[i]-d2[i]) * Math.log(d1[i]/d2[i]);
		}
		
		return klDiv/Math.log(2);
	}
	
	void storePrint(String print) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File(basepath+"stream_mining/kld_semi_"+isMCSAT+"_"+classname+"_"+isadptbinning+"_"+chunksize+"_"+kldivthreshold+".txt"), true));
			bw.write(print+"\n");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * FOREST COVER
	 * @param data
	 * @param bin
	 * @param standard
	 * @return
	 */
	HashMap<String,ArrayList<String>> dataBinForestCover(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> bin, boolean standard) {
		HashMap<String,ArrayList<String>> binData = new HashMap<String,ArrayList<String>>();
		Iterator<String> datapoint = data.keySet().iterator();
		while(datapoint.hasNext()) {
			String attrid = datapoint.next();
			ArrayList<String> newAttributes = new ArrayList<String>();
			ArrayList<String> attributes = data.get(attrid);
			for(int i=0; i<attributes.size(); i++) {
				String binval = attributes.get(i);
				if(!standard) {
					if(i==0 || i==1 || i==2 || i==3 || i==4 || i == 5 || i == 9) {
						binval = Integer.toString((int) ((Double.parseDouble(attributes.get(i))-(bin.get(i)[1]))*binSize/(bin.get(i)[0]-bin.get(i)[1])));
					} 
				} else {
					binval = getForestCoverBucket(attributes.get(i), i);
				}
				newAttributes.add(binval);
			}
			binData.put(attrid, newAttributes);
		}
		return binData;
	}
	
	String getForestCoverBucket(String val, int pos) {
		int numbuckets = 100;
		switch(pos) {
		case 0: return  Integer.toString((int) ((Double.parseDouble(val)-(1863.0))*numbuckets/(3849.0-1863.0)));
		case 1: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/360.0));
		case 2: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/61.0));
		case 3: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/1343.0));
		case 4: return Integer.toString((int) ((Double.parseDouble(val)-(-146.0))*numbuckets/(554.0+146.0)));
		case 5: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/7117.0));
		case 6: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/254.0));
		case 7: return Integer.toString((int) ((Double.parseDouble(val)-(99.0))*numbuckets/(254.0-99)));
		case 8: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/248.0));
		case 9: return Integer.toString((int) ((Double.parseDouble(val)-(0.0))*numbuckets/7173.0));
		
		default: return val;
		}
	}
	
	HashMap<Integer,double[]> getForestCoverDistribution(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> globalDistribution) {
		HashMap<Integer,double[]> dist = new HashMap<Integer,double[]>();		
		
		Iterator<String> iter = data.keySet().iterator();
		while(iter.hasNext()) {
			String dataid = iter.next();
			ArrayList<String> dataAttr = data.get(dataid);
			for(int i=0; i<dataAttr.size()-1; i++) {
				if(globalDistribution == null) {
					int attrSize = 0;
					if(i==0 || i==1 || i==2 || i==3 || i==4 || i== 5 || i== 9) {
						attrSize = binSize+1;
					} else if (i == 6 || i == 7 || i == 8) {
						attrSize = 256;
					} else if( i >= 10 && i <= 13) {
						attrSize = 2;
					} else {
						attrSize = 2;
					}
					
					double[] attr = new double[attrSize];
					for(int j=0; j<attr.length; j++) {
						attr[j] = 0;
					}
					++attr[Integer.parseInt(dataAttr.get(i))];
					dist.put(i,attr);
				} else {
					if(dist.get(i) == null) {
						double[] attr = globalDistribution.get(i);
						double[] newattr = new double[attr.length];
						for(int k=0; k<attr.length; k++) {
							newattr[k] = attr[k];
						}
						++newattr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,newattr);
					} else {
						double[] attr = dist.get(i);
						++attr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,attr);
					}
					
				}
			}
		}		
		
		return dist;
	}
	
	
	void getForestCoverPredicates(HashMap<String,ArrayList<String>> data, String filename, String truefilename, boolean train, boolean semi) {
		BufferedWriter bw = null, bwtrue = null;
		ArrayList<String> trueclass = new ArrayList<String>();
		try {
			bw = new BufferedWriter(new FileWriter(new File(filename)));			
			Iterator<String> datapoint = data.keySet().iterator();
			
			while(datapoint.hasNext()) {
				String attrid = datapoint.next();
				ArrayList<String> attributes = data.get(attrid);
				
				if(train) {
					trueclass.add(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					
					if(semi) {
						if(Math.random() < semipercentlabel) {
							bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
						}
					} else {
						bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					}
				}
				
//				if(train) {
//					bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//				}
				
				for(int i=0; i<attributes.size()-1; i++) {
					switch(i) {
					case 0: bw.write("Elevation("+attrid+", "+attributes.get(i)+")\n"); break;
					case 1: bw.write("Aspect("+attrid+", "+attributes.get(i)+")\n"); break;
					case 2: bw.write("Slope("+attrid+", "+attributes.get(i)+")\n"); break;
					case 3: bw.write("HorizontalHydrology("+attrid+", "+attributes.get(i)+")\n"); break;
					case 4: bw.write("VerticalHydrology("+attrid+", "+attributes.get(i)+")\n"); break;
					case 5: bw.write("HorizontalRoadways("+attrid+", "+attributes.get(i)+")\n"); break;
					case 6: bw.write("HillshadeNine("+attrid+", "+attributes.get(i)+")\n"); break;
					case 7: bw.write("HillshadeNoon("+attrid+", "+attributes.get(i)+")\n"); break;
					case 8: bw.write("HillshadeThree("+attrid+", "+attributes.get(i)+")\n"); break;
					case 9: bw.write("HorizontalFirePoints("+attrid+", "+attributes.get(i)+")\n"); break;
					default: 
						if(i >= 10 && i <= 13) {
							if(attributes.get(i).equals("1")) {
								bw.write("WildernessArea("+attrid+","+(i-9)+")\n");
							} else {
								bw.write("!WildernessArea("+attrid+","+(i-9)+")\n"); 
							}
						} else if(i >= 14) {
							if(attributes.get(i).equals("1")) {
								bw.write("SoilType("+attrid+","+(i-13)+")\n"); 
							} else {
								bw.write("!SoilType("+attrid+","+(i-13)+")\n"); 
							}
						}
						break;
					}
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if(train) {
			try {
				bwtrue = new BufferedWriter(new FileWriter(new File(truefilename)));
				for(int i=0; i<trueclass.size(); i++) {
					bwtrue.write(trueclass.get(i));
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(bwtrue != null) {
					try {
						bwtrue.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		
	}
	
	/**
	 * Airline Data
	 * @param data
	 * @param bin
	 * @param standard
	 * @return
	 */
	
	HashMap<String,ArrayList<String>> dataBinAirline(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> bin, boolean standard) {
		HashMap<String,ArrayList<String>> binData = new HashMap<String,ArrayList<String>>();
		Iterator<String> datapoint = data.keySet().iterator();
		while(datapoint.hasNext()) {
			String attrid = datapoint.next();
			ArrayList<String> newAttributes = new ArrayList<String>();
			ArrayList<String> attributes = data.get(attrid);
			for(int i=0; i<attributes.size(); i++) {
				String binval = attributes.get(i);
				//real valued = 1,5,6, try with only 5,6
				if(i==5 || i==6) {
					if(!standard) {
						binval = Integer.toString((int) ((Double.parseDouble(attributes.get(i))-(bin.get(i)[1]))*binSize/(bin.get(i)[0]-bin.get(i)[1])));
					} else {
						binval = getAirlineBucket(attributes.get(i), i);
					}
				}
				newAttributes.add(binval);
			}
			binData.put(attrid, newAttributes);
		}
		return binData;
	}
	
	String getAirlineBucket(String val, int pos) {
		int numbuckets = 100;
		switch(pos) {
//		case 1: return Integer.toString((int) ((Double.parseDouble(val)-(1))*numbuckets/(7813-1)));
		case 5: return Integer.toString((int) ((Double.parseDouble(val)-(10))*numbuckets/(1439-10)));
		case 6: return Integer.toString((int) ((Double.parseDouble(val)-(23))*numbuckets/(655-23)));
		default: return val;
		}
	}
	
	
	
	HashMap<Integer,double[]> getAirlineDistribution(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> globalDistribution) {
		HashMap<Integer,double[]> dist = new HashMap<Integer,double[]>();		
		
		Iterator<String> iter = data.keySet().iterator();
		while(iter.hasNext()) {
			String dataid = iter.next();
			ArrayList<String> dataAttr = data.get(dataid);
			for(int i=0; i<dataAttr.size()-1; i++) {
				if(globalDistribution == null) {
					int attrSize = 0;
					if(i==5 || i==6) {
						attrSize = binSize+1;
					} else if (i == 0) {
						attrSize = 18;
					} else if( i == 2 || i == 3) {
						attrSize = 293;
					} else if (i == 4) {
						attrSize = 8;
					} else if (i == 1) {
						attrSize = 9999;
					}
					
					double[] attr = new double[attrSize];
					for(int j=0; j<attr.length; j++) {
						attr[j] = 0;
					}
					++attr[Integer.parseInt(dataAttr.get(i))];
					dist.put(i,attr);
				} else {
					if(dist.get(i) == null) {
						double[] attr = globalDistribution.get(i);
						double[] newattr = new double[attr.length];
						for(int k=0; k<attr.length; k++) {
							newattr[k] = attr[k];
						}
						++newattr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,newattr);
					} else {
						double[] attr = dist.get(i);
						++attr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,attr);
					}
					
				}
			}
		}		
		
		return dist;
	}
	
	
	void getAirlinePredicates(HashMap<String,ArrayList<String>> data, String filename, String truefilename, boolean train, boolean semi) {
		BufferedWriter bw = null, bwtrue = null;
		ArrayList<String> trueclass = new ArrayList<String>();
		try {
			bw = new BufferedWriter(new FileWriter(new File(filename)));			
			Iterator<String> datapoint = data.keySet().iterator();
			
			while(datapoint.hasNext()) {
				String attrid = datapoint.next();
				ArrayList<String> attributes = data.get(attrid);
				
				if(train) {
					trueclass.add(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					if(semi) {
						if(Math.random() < semipercentlabel) {
							bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
						}
					} else {
						bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					}
				}
				
				for(int i=0; i<attributes.size()-1; i++) {
					switch(i) {
					case 0: bw.write("Airline("+attrid+", "+attributes.get(i)+")\n"); break;
					case 1: bw.write("Flight("+attrid+", "+attributes.get(i)+")\n"); break;
					case 2: bw.write("AirportFrom("+attrid+", "+attributes.get(i)+")\n"); break;
					case 3: bw.write("AirportTo("+attrid+", "+attributes.get(i)+")\n"); break;
					case 4: bw.write("DayOfWeek("+attrid+", "+attributes.get(i)+")\n"); break;
					case 5: bw.write("Time("+attrid+", "+attributes.get(i)+")\n"); break;
					case 6: bw.write("Length("+attrid+", "+attributes.get(i)+")\n"); break;
					
					//hybrid
//					case 5: bw.write("Time("+attrid+") "+attributes.get(i)+"\n"); break;
//					case 6: bw.write("Length("+attrid+") "+attributes.get(i)+"\n"); break;
					
					}
				}
				
//				if(semi && train) {
//					int count = (int) 0.1*data.size();
//					if(Math.random() > 0.5) {
//						if(train) {
//							bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//						}
//						
//						for(int i=0; i<attributes.size()-1; i++) {
//							switch(i) {
//							case 0: bw.write("Airline("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 1: bw.write("Flight("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 2: bw.write("AirportFrom("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 3: bw.write("AirportTo("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 4: bw.write("DayOfWeek("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 5: bw.write("Time("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 6: bw.write("Length("+attrid+", "+attributes.get(i)+")\n"); break;
//							
//							//hybrid
////							case 5: bw.write("Time("+attrid+") "+attributes.get(i)+"\n"); break;
////							case 6: bw.write("Length("+attrid+") "+attributes.get(i)+"\n"); break;
//							
//							}
//						}
//						--count;
//						if(count == 0) {
//							break;
//						}
//					}
//				}
//				
//				if(train) {
//					bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//				}
//				
//				for(int i=0; i<attributes.size()-1; i++) {
//					switch(i) {
//					case 0: bw.write("Airline("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 1: bw.write("Flight("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 2: bw.write("AirportFrom("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 3: bw.write("AirportTo("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 4: bw.write("DayOfWeek("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 5: bw.write("Time("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 6: bw.write("Length("+attrid+", "+attributes.get(i)+")\n"); break;
//					
//					//hybrid
////					case 5: bw.write("Time("+attrid+") "+attributes.get(i)+"\n"); break;
////					case 6: bw.write("Length("+attrid+") "+attributes.get(i)+"\n"); break;
//					
//					}
//				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		if(train) {
			try {
				bwtrue = new BufferedWriter(new FileWriter(new File(truefilename)));
				for(int i=0; i<trueclass.size(); i++) {
					bwtrue.write(trueclass.get(i));
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(bwtrue != null) {
					try {
						bwtrue.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	/**
	 * Poker
	 * @param data
	 * @param bin
	 * @param standard
	 * @return
	 */
	HashMap<String,ArrayList<String>> dataBinPoker(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> bin, boolean standard) {
		HashMap<String,ArrayList<String>> binData = new HashMap<String,ArrayList<String>>();
		Iterator<String> datapoint = data.keySet().iterator();
		while(datapoint.hasNext()) {
			String attrid = datapoint.next();
			ArrayList<String> newAttributes = new ArrayList<String>();
			ArrayList<String> attributes = data.get(attrid);
			for(int i=0; i<attributes.size(); i++) {
				String binval = attributes.get(i);
				newAttributes.add(binval);
			}
			binData.put(attrid, newAttributes);
		}
		return binData;
	}
	
	String getPokerBucket(String val, int pos) {
		return val;
	}
	
	
	
	HashMap<Integer,double[]> getPokerDistribution(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> globalDistribution) {
		HashMap<Integer,double[]> dist = new HashMap<Integer,double[]>();		
		
		Iterator<String> iter = data.keySet().iterator();
		while(iter.hasNext()) {
			String dataid = iter.next();
			ArrayList<String> dataAttr = data.get(dataid);
			for(int i=0; i<dataAttr.size()-1; i++) {
				if(globalDistribution == null) {
					int attrSize = 0;
					if(i==1 || i==3 || i==5 || i==7 || i == 9) {
						attrSize = 14;
					} else {
						attrSize = 5;
					}
					
					double[] attr = new double[attrSize];
					for(int j=0; j<attr.length; j++) {
						attr[j] = 0;
					}
					++attr[Integer.parseInt(dataAttr.get(i))];
					dist.put(i,attr);
				} else {
					if(dist.get(i) == null) {
						double[] attr = globalDistribution.get(i);
						double[] newattr = new double[attr.length];
						for(int k=0; k<attr.length; k++) {
							newattr[k] = attr[k];
						}
						++newattr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,newattr);
					} else {
						double[] attr = dist.get(i);
						++attr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,attr);
					}
					
				}
			}
		}		
		
		return dist;
	}
	
	
	void getPokerPredicates(HashMap<String,ArrayList<String>> data, String filename, String truefilename, boolean train, boolean semi) {
		BufferedWriter bw = null, bwtrue=null;
		ArrayList<String> trueclass = new ArrayList<String>();
		try {
			bw = new BufferedWriter(new FileWriter(new File(filename)));
			Iterator<String> datapoint = data.keySet().iterator();
			
			while(datapoint.hasNext()) {
				String attrid = datapoint.next();
				ArrayList<String> attributes = data.get(attrid);
				
				if(train) {
					trueclass.add(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					if(semi) {
						if(Math.random() < semipercentlabel) {
							bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
						}
					} else {
						bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					}
				}
				
				for(int i=0; i<attributes.size()-1; i++) {
					switch(i) {
					case 0: bw.write("SOne("+attrid+", "+attributes.get(i)+")\n"); break;
					case 1: bw.write("COne("+attrid+", "+attributes.get(i)+")\n"); break;
					case 2: bw.write("STwo("+attrid+", "+attributes.get(i)+")\n"); break;
					case 3: bw.write("CTwo("+attrid+", "+attributes.get(i)+")\n"); break;
					case 4: bw.write("SThree("+attrid+", "+attributes.get(i)+")\n"); break;
					case 5: bw.write("CThree("+attrid+", "+attributes.get(i)+")\n"); break;
					case 6: bw.write("SFour("+attrid+", "+attributes.get(i)+")\n"); break;
					case 7: bw.write("CFour("+attrid+", "+attributes.get(i)+")\n"); break;
					case 8: bw.write("SFive("+attrid+", "+attributes.get(i)+")\n"); break;
					case 9: bw.write("CFive("+attrid+", "+attributes.get(i)+")\n"); break;
					
					}
				}
				
//				if(semi && train) {
//					int count = (int) 0.5*data.size();
//					if(Math.random() > 0.5) {
//						if(train) {
//							bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//						}
//						
//						for(int i=0; i<attributes.size()-1; i++) {
//							switch(i) {
//							case 0: bw.write("SOne("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 1: bw.write("COne("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 2: bw.write("STwo("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 3: bw.write("CTwo("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 4: bw.write("SThree("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 5: bw.write("CThree("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 6: bw.write("SFour("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 7: bw.write("CFour("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 8: bw.write("SFive("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 9: bw.write("CFive("+attrid+", "+attributes.get(i)+")\n"); break;
//							
//							}
//						}
//						--count;
//						if(count == 0) {
//							break;
//						}
//					}
//				} else {
//					if(train) {
//						bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//					}
//					
//					for(int i=0; i<attributes.size()-1; i++) {
//						switch(i) {
//						case 0: bw.write("SOne("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 1: bw.write("COne("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 2: bw.write("STwo("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 3: bw.write("CTwo("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 4: bw.write("SThree("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 5: bw.write("CThree("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 6: bw.write("SFour("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 7: bw.write("CFour("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 8: bw.write("SFive("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 9: bw.write("CFive("+attrid+", "+attributes.get(i)+")\n"); break;
//						
//						}
//					}
//				}
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if(train) {
			try {
				bwtrue = new BufferedWriter(new FileWriter(new File(truefilename)));
				for(int i=0; i<trueclass.size(); i++) {
					bwtrue.write(trueclass.get(i));
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(bwtrue != null) {
					try {
						bwtrue.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	
	/**
	 * PAMAP Data
	 * @param data
	 * @param bin
	 * @param standard
	 * @return
	 */
	
	HashMap<String,ArrayList<String>> dataBinPAMAP(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> bin, boolean standard) {
		HashMap<String,ArrayList<String>> binData = new HashMap<String,ArrayList<String>>();
		Iterator<String> datapoint = data.keySet().iterator();
		while(datapoint.hasNext()) {
			String attrid = datapoint.next();
			ArrayList<String> newAttributes = new ArrayList<String>();
			ArrayList<String> attributes = data.get(attrid);
			for(int i=0; i<attributes.size(); i++) {
				String binval = attributes.get(i);
				
				if(i != attributes.size()-1) {
					if(!standard) {
						binval = Integer.toString((int) ((Double.parseDouble(attributes.get(i))-(bin.get(i)[1]))*binSize/(bin.get(i)[0]-bin.get(i)[1])));
					} else {
//						binval = getAirlineBucket(attributes.get(i), i);
					}
				} else {
					binval = attributes.get(i).substring(0, 1).toUpperCase()+attributes.get(i).substring(1);
				}
				
				newAttributes.add(binval);
			}
			binData.put(attrid, newAttributes);
		}
		return binData;
	}
	
	
	
	
	HashMap<Integer,double[]> getPAMAPDistribution(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> globalDistribution) {
		HashMap<Integer,double[]> dist = new HashMap<Integer,double[]>();		
		
		Iterator<String> iter = data.keySet().iterator();
		while(iter.hasNext()) {
			String dataid = iter.next();
			ArrayList<String> dataAttr = data.get(dataid);
			for(int i=0; i<dataAttr.size()-1; i++) {
				if(globalDistribution == null) {
					int attrSize = binSize+1;
					
					double[] attr = new double[attrSize];
					for(int j=0; j<attr.length; j++) {
						attr[j] = 0;
					}
					++attr[Integer.parseInt(dataAttr.get(i))];
					dist.put(i,attr);
				} else {
					if(dist.get(i) == null) {
						double[] attr = globalDistribution.get(i);
						double[] newattr = new double[attr.length];
						for(int k=0; k<attr.length; k++) {
							newattr[k] = attr[k];
						}
						++newattr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,newattr);
					} else {
						double[] attr = dist.get(i);
						++attr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,attr);
					}
					
				}
			}
		}		
		
		return dist;
	}
	
	
	void getPAMAPPredicates(HashMap<String,ArrayList<String>> data, String filename, boolean train, boolean semi) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File(filename)));
			
			Iterator<String> datapoint = data.keySet().iterator();
			
			while(datapoint.hasNext()) {
				String attrid = datapoint.next();
				ArrayList<String> attributes = data.get(attrid);
				
//				if(semi && train) {
//					int count = (int) 0.1*data.size();
//					if(Math.random() > 0.5) {
//						if(train) {
//							bw.write("Delayed("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//						}
//						
//						for(int i=0; i<attributes.size()-1; i++) {
//							switch(i) {
//							case 0: bw.write("Airline("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 1: bw.write("Flight("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 2: bw.write("AirportFrom("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 3: bw.write("AirportTo("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 4: bw.write("DayOfWeek("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 5: bw.write("Time("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 6: bw.write("Length("+attrid+", "+attributes.get(i)+")\n"); break;
//							
//							//hybrid
////							case 5: bw.write("Time("+attrid+") "+attributes.get(i)+"\n"); break;
////							case 6: bw.write("Length("+attrid+") "+attributes.get(i)+"\n"); break;
//							
//							}
//						}
//						--count;
//						if(count == 0) {
//							break;
//						}
//					}
//				}
				
				if(train) {
					bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
				}
				
				for(int i=0; i<attributes.size()-1; i++) {
					switch(i) {
//					case 0: bw.write("TimeStamp("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 1: bw.write("HeartRate("+attrid+", "+attributes.get(i)+")\n"); break;
//					
//					case 2: bw.write("HandTemp("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 3: bw.write("HandXAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 4: bw.write("HandYAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 5: bw.write("HandZAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 6: bw.write("HandXAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 7: bw.write("HandYAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 8: bw.write("HandZAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 9: bw.write("HandXGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 10: bw.write("HandYGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 11: bw.write("HandZGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 12: bw.write("HandXMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 13: bw.write("HandYMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 14: bw.write("HandZMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					
//					case 19: bw.write("ChestTemp("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 20: bw.write("ChestXAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 21: bw.write("ChestYAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 22: bw.write("ChestZAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 23: bw.write("ChestXAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 24: bw.write("ChestYAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 25: bw.write("ChestZAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 26: bw.write("ChestXGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 27: bw.write("ChestYGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 28: bw.write("ChestZGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 29: bw.write("ChestXMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 30: bw.write("ChestYMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 31: bw.write("ChestZMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					
//					case 36: bw.write("AnkleTemp("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 37: bw.write("AnkleXAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 38: bw.write("AnkleYAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 39: bw.write("AnkleZAcc16("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 40: bw.write("AnkleXAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 41: bw.write("AnkleYAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 42: bw.write("AnkleZAcc06("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 43: bw.write("AnkleXGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 44: bw.write("AnkleYGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 45: bw.write("AnkleZGyro("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 46: bw.write("AnkleXMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 47: bw.write("AnkleYMag("+attrid+", "+attributes.get(i)+")\n"); break;
//					case 48: bw.write("AnkleZMag("+attrid+", "+attributes.get(i)+")\n"); break;
					
					case 0: bw.write("att1("+attrid+", "+attributes.get(i)+")\n"); break;
					case 1: bw.write("att2("+attrid+", "+attributes.get(i)+")\n"); break;
					case 2: bw.write("att3("+attrid+", "+attributes.get(i)+")\n"); break;
					case 3: bw.write("att4("+attrid+", "+attributes.get(i)+")\n"); break;
					case 4: bw.write("att5("+attrid+", "+attributes.get(i)+")\n"); break;
					case 5: bw.write("att6("+attrid+", "+attributes.get(i)+")\n"); break;
					case 6: bw.write("att7("+attrid+", "+attributes.get(i)+")\n"); break;
					case 7: bw.write("att8("+attrid+", "+attributes.get(i)+")\n"); break;
					case 8: bw.write("att9("+attrid+", "+attributes.get(i)+")\n"); break;
					case 9: bw.write("att10("+attrid+", "+attributes.get(i)+")\n"); break;
					case 10: bw.write("att11("+attrid+", "+attributes.get(i)+")\n"); break;
					case 11: bw.write("att12("+attrid+", "+attributes.get(i)+")\n"); break;
					case 12: bw.write("att13("+attrid+", "+attributes.get(i)+")\n"); break;
					case 13: bw.write("att14("+attrid+", "+attributes.get(i)+")\n"); break;
					case 14: bw.write("att15("+attrid+", "+attributes.get(i)+")\n"); break;
					case 15: bw.write("att16("+attrid+", "+attributes.get(i)+")\n"); break;
					case 16: bw.write("att17("+attrid+", "+attributes.get(i)+")\n"); break;
					case 17: bw.write("att18("+attrid+", "+attributes.get(i)+")\n"); break;
					case 18: bw.write("att19("+attrid+", "+attributes.get(i)+")\n"); break;
					case 19: bw.write("att20("+attrid+", "+attributes.get(i)+")\n"); break;
					case 20: bw.write("att21("+attrid+", "+attributes.get(i)+")\n"); break;
					case 21: bw.write("att22("+attrid+", "+attributes.get(i)+")\n"); break;
					case 22: bw.write("att23("+attrid+", "+attributes.get(i)+")\n"); break;
					case 23: bw.write("att24("+attrid+", "+attributes.get(i)+")\n"); break;
					case 24: bw.write("att25("+attrid+", "+attributes.get(i)+")\n"); break;
					case 25: bw.write("att26("+attrid+", "+attributes.get(i)+")\n"); break;
					case 26: bw.write("att27("+attrid+", "+attributes.get(i)+")\n"); break;
					case 27: bw.write("att28("+attrid+", "+attributes.get(i)+")\n"); break;
					case 28: bw.write("att29("+attrid+", "+attributes.get(i)+")\n"); break;
					case 29: bw.write("att30("+attrid+", "+attributes.get(i)+")\n"); break;
					case 30: bw.write("att31("+attrid+", "+attributes.get(i)+")\n"); break;
					case 31: bw.write("att32("+attrid+", "+attributes.get(i)+")\n"); break;
					case 32: bw.write("att33("+attrid+", "+attributes.get(i)+")\n"); break;
					case 33: bw.write("att34("+attrid+", "+attributes.get(i)+")\n"); break;
					case 34: bw.write("att35("+attrid+", "+attributes.get(i)+")\n"); break;
					case 35: bw.write("att36("+attrid+", "+attributes.get(i)+")\n"); break;
					case 36: bw.write("att37("+attrid+", "+attributes.get(i)+")\n"); break;
					case 37: bw.write("att38("+attrid+", "+attributes.get(i)+")\n"); break;
					case 38: bw.write("att39("+attrid+", "+attributes.get(i)+")\n"); break;
					case 39: bw.write("att40("+attrid+", "+attributes.get(i)+")\n"); break;
					case 40: bw.write("att41("+attrid+", "+attributes.get(i)+")\n"); break;
					case 41: bw.write("att42("+attrid+", "+attributes.get(i)+")\n"); break;
					case 42: bw.write("att43("+attrid+", "+attributes.get(i)+")\n"); break;
					case 43: bw.write("att44("+attrid+", "+attributes.get(i)+")\n"); break;
					case 44: bw.write("att45("+attrid+", "+attributes.get(i)+")\n"); break;
					case 45: bw.write("att46("+attrid+", "+attributes.get(i)+")\n"); break;
					case 46: bw.write("att47("+attrid+", "+attributes.get(i)+")\n"); break;
					case 47: bw.write("att48("+attrid+", "+attributes.get(i)+")\n"); break;
					case 48: bw.write("att49("+attrid+", "+attributes.get(i)+")\n"); break;
					case 49: bw.write("att50("+attrid+", "+attributes.get(i)+")\n"); break;
					default: break;		
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	/**
	 * Synthetic Data - LED Drift
	 * @param data
	 * @param bin
	 * @param standard
	 * @return
	 */
	HashMap<String,ArrayList<String>> dataBinLED(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> bin, boolean standard) {
		HashMap<String,ArrayList<String>> binData = new HashMap<String,ArrayList<String>>();
		Iterator<String> datapoint = data.keySet().iterator();
		while(datapoint.hasNext()) {
			String attrid = datapoint.next();
			ArrayList<String> newAttributes = new ArrayList<String>();
			ArrayList<String> attributes = data.get(attrid);
			for(int i=0; i<attributes.size(); i++) {
				String binval = attributes.get(i);
				newAttributes.add(binval);
			}
			binData.put(attrid, newAttributes);
		}
		return binData;
	}
	
	String getLEDBucket(String val, int pos) {
		return val;
	}
	
	
	
	HashMap<Integer,double[]> getLEDDistribution(HashMap<String,ArrayList<String>> data, HashMap<Integer,double[]> globalDistribution) {
		HashMap<Integer,double[]> dist = new HashMap<Integer,double[]>();		
		
		Iterator<String> iter = data.keySet().iterator();
		while(iter.hasNext()) {
			String dataid = iter.next();
			ArrayList<String> dataAttr = data.get(dataid);
			for(int i=0; i<dataAttr.size()-1; i++) {
				if(globalDistribution == null) {
					int attrSize = 0;
					if(i==1 || i==3 || i==5 || i==7 || i == 9) {
						attrSize = 14;
					} else {
						attrSize = 5;
					}
					
					double[] attr = new double[attrSize];
					for(int j=0; j<attr.length; j++) {
						attr[j] = 0;
					}
					++attr[Integer.parseInt(dataAttr.get(i))];
					dist.put(i,attr);
				} else {
					if(dist.get(i) == null) {
						double[] attr = globalDistribution.get(i);
						double[] newattr = new double[attr.length];
						for(int k=0; k<attr.length; k++) {
							newattr[k] = attr[k];
						}
						++newattr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,newattr);
					} else {
						double[] attr = dist.get(i);
						++attr[Integer.parseInt(dataAttr.get(i))];
						dist.put(i,attr);
					}
					
				}
			}
		}		
		
		return dist;
	}
	
	
	void getLEDPredicates(HashMap<String,ArrayList<String>> data, String filename, String truefilename, boolean train, boolean semi) {
		BufferedWriter bw = null, bwtrue = null;
		ArrayList<String> trueclass = new ArrayList<String>();
		try {
			bw = new BufferedWriter(new FileWriter(new File(filename)));
			Iterator<String> datapoint = data.keySet().iterator();
			
			while(datapoint.hasNext()) {
				String attrid = datapoint.next();
				ArrayList<String> attributes = data.get(attrid);
				
				if(train) {
					trueclass.add(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					if(semi) {
						if(Math.random() < semipercentlabel) {
							bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
						}
					} else {
						bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
					}
				}
				
				for(int i=0; i<attributes.size()-1; i++) {
					switch(i) {
					case 0: bw.write("Att1("+attrid+", "+attributes.get(i)+")\n"); break;
					case 1: bw.write("Att2("+attrid+", "+attributes.get(i)+")\n"); break;
					case 2: bw.write("Att3("+attrid+", "+attributes.get(i)+")\n"); break;
					case 3: bw.write("Att4("+attrid+", "+attributes.get(i)+")\n"); break;
					case 4: bw.write("Att5("+attrid+", "+attributes.get(i)+")\n"); break;
					case 5: bw.write("Att6("+attrid+", "+attributes.get(i)+")\n"); break;
					case 6: bw.write("Att7("+attrid+", "+attributes.get(i)+")\n"); break;
					}
				}
				
//				if(semi && train) {
//					int count = (int) 0.5*data.size();
//					if(Math.random() > 0.5) {
//						if(train) {
//							bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//						}
//						
//						for(int i=0; i<attributes.size()-1; i++) {
//							switch(i) {
//							case 0: bw.write("Att1("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 1: bw.write("Att2("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 2: bw.write("Att3("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 3: bw.write("Att4("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 4: bw.write("Att5("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 5: bw.write("Att6("+attrid+", "+attributes.get(i)+")\n"); break;
//							case 6: bw.write("Att7("+attrid+", "+attributes.get(i)+")\n"); break;
//							
//							
//							}
//						}
//						--count;
//						if(count == 0) {
//							break;
//						}
//					}
//				} else {
//					if(train) {
//						bw.write(classname+"("+attrid+","+attributes.get(attributes.size()-1)+")\n");
//					}
//					
//					for(int i=0; i<attributes.size()-1; i++) {
//						switch(i) {
//						case 0: bw.write("Att1("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 1: bw.write("Att2("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 2: bw.write("Att3("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 3: bw.write("Att4("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 4: bw.write("Att5("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 5: bw.write("Att6("+attrid+", "+attributes.get(i)+")\n"); break;
//						case 6: bw.write("Att7("+attrid+", "+attributes.get(i)+")\n"); break;
//						}
//					}
//				}
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		if(train) {
			try {
				bwtrue = new BufferedWriter(new FileWriter(new File(truefilename)));
				for(int i=0; i<trueclass.size(); i++) {
					bwtrue.write(trueclass.get(i));
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(bwtrue != null) {
					try {
						bwtrue.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	
	
	
	
}
