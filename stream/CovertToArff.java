import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class CovertToArff {
	public static void main(String[] args) {
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/covtype.data")));
			bw = new BufferedWriter(new FileWriter(new File("covtype_small.arff")));
			
			StringBuffer strbuf = new StringBuffer();
			strbuf.append("@RELATION AIRLINE\n");
			strbuf.append("@ATTRIBUTE Elevation NUMERIC\n");
			strbuf.append("@ATTRIBUTE Aspect NUMERIC\n");
			strbuf.append("@ATTRIBUTE Slope NUMERIC\n");
			strbuf.append("@ATTRIBUTE Horizontal_Distance_To_Hydrology NUMERIC\n");
			strbuf.append("@ATTRIBUTE Vertical_Distance_To_Hydrology NUMERIC\n");
			strbuf.append("@ATTRIBUTE Horizontal_Distance_To_Roadways NUMERIC\n");
			strbuf.append("@ATTRIBUTE Hillshade_9am NUMERIC\n");
			strbuf.append("@ATTRIBUTE Hillshade_Noon NUMERIC\n");
			strbuf.append("@ATTRIBUTE Hillshade_3pm NUMERIC\n");
			strbuf.append("@ATTRIBUTE Horizontal_Distance_To_Fire_Points NUMERIC\n");
			strbuf.append("@ATTRIBUTE Wilderness_Area1 NUMERIC\n");
			strbuf.append("@ATTRIBUTE Wilderness_Area2 NUMERIC\n");
			strbuf.append("@ATTRIBUTE Wilderness_Area3 NUMERIC\n");
			strbuf.append("@ATTRIBUTE Wilderness_Area4 NUMERIC\n");
			for(int i=1; i<=40; i++)
				strbuf.append("@ATTRIBUTE Soil_Type"+i+" NUMERIC\n");
			
			strbuf.append("@ATTRIBUTE Cover_Type {1,2,3,4,5,6,7}\n\n");
			strbuf.append("@DATA\n");
			
			bw.write(strbuf.toString());
			
			String read = "";
			int count = 0;
			
			while((read=br.readLine()) != null) {
				bw.write(read+"\n");
				++count;
				if(count == 100000) {
					break;
				}
				
			}
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
