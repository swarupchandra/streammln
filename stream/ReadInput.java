
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ReadInput {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BufferedReader br = null;
		int initial = 0, numfile = 0;
		int numData = 100000, chunksize = 1000;
		
		try {
			br = new BufferedReader(new FileReader(new File("/home/swarup/Documents/dataset/")));
			String read = "";
			boolean start = false;
			int count = 0, valID = 0;
			ArrayList<Double> largest = new ArrayList<Double>(), smallest = new ArrayList<Double>();
			
			
			ArrayList<String> givenclass = new ArrayList<String>(),
					attribute = new ArrayList<String>();
			ArrayList<ArrayList<Double>> values = new ArrayList<ArrayList<Double>>();
			HashMap<String,Integer> classname = new HashMap<String,Integer>();
			
			ArrayList<Double> bucketSize = new ArrayList<Double>();
			
			while((read=br.readLine()) != null) {
				if(!start) {
					if(read.startsWith("@ATTRIBUTE")) {
						attribute.add(read.split(" ")[1]);
					} else if(read.equals("@DATA")) {
						start = true;
						for(int i=0; i<attribute.size(); i++) {
							largest.add(-1000.0);
							smallest.add(1000.0);
						}
					}
				} else {
					String[] split = read.split(",");
					if(!split[split.length-1].contains("invalid") && !split[split.length-1].contains("other")) {
						
//						if(count >= initial) {
							givenclass.add(split[split.length-1]);
							if(classname.get(split[split.length-1]) == null) {
								classname.put(split[split.length-1], 1);
							} else {
								int i = classname.get(split[split.length-1]);
								classname.put(split[split.length-1], ++i);
							}
							
							ArrayList<Double> temp = new ArrayList<Double>();
							for(int i=0; i<split.length-1; i++) {
								temp.add(Double.parseDouble(split[i]));
							}
							values.add(temp);
							largest = getLargest(largest, temp);
							smallest = getSmallest(smallest,temp);
//						}
						++count;
					}
				}
				
				if(count != 0 && count%chunksize == 0) {
					
					System.out.println("Count="+count+"; ValueSize="+values.size());
					
					//0=train; 1=test
					++numfile;
					writefile(0, givenclass, attribute, values, initial, numfile);
					writefile(1, givenclass, attribute, values, initial, numfile);
					
					//reset
					givenclass.clear();
//					givenclass = new ArrayList<String>();
					values.clear();
//					values = new HashMap<Integer,ArrayList<String>>();
					valID = 0;
					
					initial += chunksize;
					
					if(initial == numData) {
						Iterator<String> iter = classname.keySet().iterator();
						while(iter.hasNext()) {
							System.out.println("-"+iter.next());
						}
						
						break;
					}
				}
				
				
			}
			
			for(int i=0; i<largest.size(); i++) {
				System.out.println(attribute.get(i)+" - "+largest.get(i) + " - " + smallest.get(i) + " - " + (largest.get(i) - smallest.get(i)));
			}
			
			for(int i=0; i<largest.size(); i++) {
				System.out.println("case "+i+": return (int) ((val-("+smallest.get(i)+"))/"+(largest.get(i)-smallest.get(i))+");");
			}
			
			for(String temp : classname.keySet()) {
				System.out.println(temp+" : "+classname.get(temp));
			}
			
			
			
			
			
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println("END");
		
		
		
		

	}
	
	

	public static void writefile(int type,ArrayList<String> givenclass, ArrayList<String> attribute, ArrayList<ArrayList<Double>> values, int initial, int numfile) {
		BufferedWriter bw = null;
		try {
			
			if(type == 0) {
				bw = new BufferedWriter(new FileWriter(new File("training_data/train"+numfile+".db")));
				
				for(int i=0; i<givenclass.size(); i++) {
					String temp = givenclass.get(i);
					temp = temp.substring(0,1).toUpperCase()+temp.substring(1);
					bw.write("Class(A"+(i+initial)+","+temp+")\n");
				}
				
				bw.write("\n");
				
			} else {
				bw = new BufferedWriter(new FileWriter(new File("testing_data/test"+numfile+".db")));
			}
			
			
			for(int k=1; k<attribute.size()-1; k++) {
				for(int i=0; i<values.size(); i++) {
					if(!givenclass.get(i).contains("invalid")) {
						if(attribute.get(k).equals("HeartRate_bpm")) {
							bw.write("HeartRate(A"+(i+initial)+", "+values.get(i).get(k)+")\n");
						} 
						
						else if(attribute.get(k).equals("IMUHand_Temperature_C")) {
							bw.write("HandTemp(A"+(i+initial)+", "+values.get(i).get(k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_X_Accel_16g")) {
							bw.write("HandXAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Y_Accel_16g")) {
							bw.write("HandYAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Z_Accel_16g")) {
							bw.write("HandZAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_X_Accel_06g")) {
							bw.write("HandXAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Y_Accel_06g")) {
							bw.write("HandYAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Z_Accel_06g")) {
							bw.write("HandZAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_X_Gyro_rad_s")) {
							bw.write("HandXGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Y_Gyro_rad_s")) {
							bw.write("HandYGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Z_Gyro_rad_s")) {
							bw.write("HandZGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_X_Magnetometer_uT")) {
							bw.write("HandXMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Y_Magnetometer_uT")) {
							bw.write("HandYMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUHand_Z_Magnetometer_uT")) {
							bw.write("HandZMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} 
						
						else if(attribute.get(k).equals("IMUChest_Temperature_C")) {
							bw.write("ChestTemp(A"+(i+initial)+", "+values.get(i).get(k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_X_Accel_16g")) {
							bw.write("ChestXAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Y_Accel_16g")) {
							bw.write("ChestYAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Z_Accel_16g")) {
							bw.write("ChestZAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_X_Accel_06g")) {
							bw.write("ChestXAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Y_Accel_06g")) {
							bw.write("ChestYAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Z_Accel_06g")) {
							bw.write("ChestZAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_X_Gyro_rad_s")) {
							bw.write("ChestXGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Y_Gyro_rad_s")) {
							bw.write("ChestYGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Z_Gyro_rad_s")) {
							bw.write("ChestZGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_X_Magnetometer_uT")) {
							bw.write("ChestXMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Y_Magnetometer_uT")) {
							bw.write("ChestYMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUChest_Z_Magnetometer_uT")) {
							bw.write("ChestZMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} 
						
						else if(attribute.get(k).equals("IMUAnkle_Temperature_C")) {
							bw.write("AnkleTemp(A"+(i+initial)+", "+values.get(i).get(k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_X_Accel_16g")) {
							bw.write("AnkleXAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Y_Accel_16g")) {
							bw.write("AnkleYAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Z_Accel_16g")) {
							bw.write("AnkleZAcc16(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_X_Accel_06g")) {
							bw.write("AnkleXAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Y_Accel_06g")) {
							bw.write("AnkleYAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Z_Accel_06g")) {
							bw.write("AnkleZAcc06(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_X_Gyro_rad_s")) {
							bw.write("AnkleXGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Y_Gyro_rad_s")) {
							bw.write("AnkleYGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Z_Gyro_rad_s")) {
							bw.write("AnkleZGyro(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_X_Magnetometer_uT")) {
							bw.write("AnkleXMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Y_Magnetometer_uT")) {
							bw.write("AnkleYMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} else if(attribute.get(k).equals("IMUAnkle_Z_Magnetometer_uT")) {
							bw.write("AnkleZMag(A"+(i+initial)+", "+getBucket(values.get(i).get(k),k)+")\n");
						} 	
										
					}
					
				}
				bw.write("\n");
			}
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static ArrayList<Double> getLargest(ArrayList<Double> largest, ArrayList<Double> value) {
		ArrayList<Double> large = new ArrayList<Double>();
		for(int i=0; i<value.size(); i++) {
			if(largest.get(i) < value.get(i)) {
				large.add(value.get(i));
			} else {
				large.add(largest.get(i));
			}
		}
		return large;
	}
	
	public static ArrayList<Double> getSmallest(ArrayList<Double> smallest, ArrayList<Double> value) {
		ArrayList<Double> small = new ArrayList<Double>();
		for(int i=0; i<value.size(); i++) {
			if(smallest.get(i) > value.get(i)) {
				small.add(value.get(i));
			} else {
				small.add(smallest.get(i));
			}
		}
		return small;
	}
	
	public static int getBucket(double val, int pos) {
		int numbuckets = 100;
		switch(pos) {
		case 3: return (int) ((val-(-41.2779))*numbuckets/52.9855);
		case 4: return (int) ((val-(-15.0973))*numbuckets/35.9371);
		case 5: return (int) ((val-(-16.8267))*numbuckets/38.6122);
		case 6: return (int) ((val-(-39.17))*numbuckets/50.9216);
		case 7: return (int) ((val-(-15.1911))*numbuckets/36.5604);
		case 8: return (int) ((val-(-14.9934))*numbuckets/36.873599999999996);
		case 9: return (int) ((val-(-8.27393))*numbuckets/15.37838);
		case 10: return (int) ((val-(-5.31223))*numbuckets/9.83183);
		case 11: return (int) ((val-(-4.64501))*numbuckets/8.40889);
		case 12: return (int) ((val-(-68.5129))*numbuckets/148.85309999999998);
		case 13: return (int) ((val-(-160.787))*numbuckets/220.3562);
		case 14: return (int) ((val-(-133.092))*numbuckets/181.94260000000003);
		case 20: return (int) ((val-(-6.71015))*numbuckets/19.59625);
		case 21: return (int) ((val-(-3.42727))*numbuckets/15.86987);
		case 22: return (int) ((val-(-13.2587))*numbuckets/24.4797);
		case 23: return (int) ((val-(-6.85093))*numbuckets/19.51703);
		case 24: return (int) ((val-(-3.35388))*numbuckets/15.79098);
		case 25: return (int) ((val-(-13.076))*numbuckets/24.557000000000002);
		case 26: return (int) ((val-(-2.26224))*numbuckets/4.19571);
		case 27: return (int) ((val-(-2.36119))*numbuckets/4.58736);
		case 28: return (int) ((val-(-1.24275))*numbuckets/3.00805);
		case 29: return (int) ((val-(-45.4474))*numbuckets/96.9493);
		case 30: return (int) ((val-(-76.42))*numbuckets/95.6417);
		case 31: return (int) ((val-(-66.6847))*numbuckets/137.14870000000002);
		case 37: return (int) ((val-(-4.40662))*numbuckets/41.85892);
		case 38: return (int) ((val-(-29.7182))*numbuckets/70.6293);
		case 39: return (int) ((val-(-17.9975))*numbuckets/33.8036);
		case 40: return (int) ((val-(-4.05487))*numbuckets/42.922270000000005);
		case 41: return (int) ((val-(-22.7641))*numbuckets/62.0435);
		case 42: return (int) ((val-(-17.5368))*numbuckets/34.036500000000004);
		case 43: return (int) ((val-(-5.76575))*numbuckets/9.546949999999999);
		case 44: return (int) ((val-(-4.4146))*numbuckets/6.81135);
		case 45: return (int) ((val-(-5.47967))*numbuckets/8.941189999999999);
		case 46: return (int) ((val-(-126.418))*numbuckets/147.8072);
		case 47: return (int) ((val-(-81.9909))*numbuckets/149.3397);
		case 48: return (int) ((val-(-87.0971))*numbuckets/161.9957);
		
		default: return 0;
		}
	}
}
