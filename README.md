README

Stream mining using Statistical Relational Learning.

Require:
1. MLN Files
2. Dataset path
3. Alchemy folders (training_data, testing_data, result, output, inputstream, ensemble-in, ensemble-out)


Uses KLDistance to check if weight learning is required.
Currently works for ForestCover, Airline, Poker and SyntheticLED datasets.